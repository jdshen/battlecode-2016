package team031.lookups;

import battlecode.common.Direction;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jdshen on 1/6/16.
 */
public class PreAttackerLookupsTest {
    @Test
    public void testUnpack() throws Exception {
        long packed = 0;
        int[] unpacked;
        packed += AttackerLookups.getDirs(48, 7, 0);
        packed += AttackerLookups.getDirs(48, 0, 7);
        packed += AttackerLookups.getDirs(2, 2, 2);
        packed += AttackerLookups.getDirs(13, -3, -3);
        packed += AttackerLookups.getDirs(13, 1, -1);

        unpacked = AttackerLookups.unpack(packed);
        Assert.assertEquals(unpacked[Direction.NORTH.ordinal()], 2);
        Assert.assertEquals(unpacked[Direction.NORTH_EAST.ordinal()], 2);
        Assert.assertEquals(unpacked[Direction.EAST.ordinal()], 2);
        Assert.assertEquals(unpacked[Direction.SOUTH_EAST.ordinal()], 4);
        Assert.assertEquals(unpacked[Direction.SOUTH.ordinal()], 2);
        Assert.assertEquals(unpacked[Direction.SOUTH_WEST.ordinal()], 2);
        Assert.assertEquals(unpacked[Direction.WEST.ordinal()], 2);
        Assert.assertEquals(unpacked[Direction.NORTH_WEST.ordinal()], 2);
        Assert.assertEquals(unpacked[Direction.OMNI.ordinal()], 1);
    }

    @Test
    public void testUnpack2() throws Exception {
        long packed = 0;
        int[] unpacked;
        packed += AttackerLookups.getDirs(48, 7, 0);
        packed += AttackerLookups.getDirs(48, 0, 7);
        packed += AttackerLookups.getDirs(2, 2, 2);
        packed += AttackerLookups.getDirs(13, -3, -3);
        packed += AttackerLookups.getDirs(13, 1, -1);
        packed += AttackerLookups.getDirs(13, -2, -2);

        unpacked = AttackerLookups.unpack(packed);
        Assert.assertEquals(unpacked[Direction.NORTH.ordinal()], 3);
        Assert.assertEquals(unpacked[Direction.NORTH_EAST.ordinal()], 3);
        Assert.assertEquals(unpacked[Direction.EAST.ordinal()], 3);
        Assert.assertEquals(unpacked[Direction.SOUTH_EAST.ordinal()], 4);
        Assert.assertEquals(unpacked[Direction.SOUTH.ordinal()], 3);
        Assert.assertEquals(unpacked[Direction.SOUTH_WEST.ordinal()], 3);
        Assert.assertEquals(unpacked[Direction.WEST.ordinal()], 3);
        Assert.assertEquals(unpacked[Direction.NORTH_WEST.ordinal()], 3);
        Assert.assertEquals(unpacked[Direction.OMNI.ordinal()], 2);
    }
}