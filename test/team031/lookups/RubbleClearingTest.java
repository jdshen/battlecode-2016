package team031.lookups;

import org.junit.Assert;
import org.junit.Test;

public class RubbleClearingTest {

    @Test
    public void testGetClears() throws Exception {
        Assert.assertTrue(RubbleClearing.getClears(1000000) <= 170);
    }
}