package team031.messaging;

import battlecode.common.MapLocation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by jdshen on 1/6/16.
 */
public class MessagerTest {
    @Before
    public void setLoc() {
        Messager.X = -10000;
        Messager.Y = 10000;
    }

    @Test
    public void testID() {
        Message m = new Message();
        m.type = MessageType.ACTOR;
        m.id = 131;
        int[] enc = Messager.encode(m);
        Message n = Messager.decode(enc[0], enc[1]);
        Assert.assertEquals(m.type, n.type);
        Assert.assertEquals(m.id, n.id);
    }

    @Test
    public void testLocs() {
        Message m = new Message();
        m.type = MessageType.ENEMIES_AT;
        m.locs = new MapLocation[] {
            new MapLocation(-10000 + 99, 10000 - 99),
            new MapLocation(-10000 + 99, 10000),
            new MapLocation(-10000, 10000 - 99),
        };

        int[] enc = Messager.encode(m);
        Message n = Messager.decode(enc[0], enc[1]);
        Assert.assertEquals(m.type, n.type);
        Assert.assertArrayEquals(m.locs, n.locs);
    }


    @Test
    public void testIDandLocs() {
        Message m = new Message();
        m.type = MessageType.ZOMBIE_DEN_RELAY;
        m.locs = new MapLocation[] {
            new MapLocation(-10000 + 99, 10000 - 99),
            new MapLocation(-10000 + 99, 10000)
        };
        m.id = Messager.hash(31911, 432);

        int[] enc = Messager.encode(m);
        Message n = Messager.decode(enc[0], enc[1]);
        Assert.assertEquals(m.type, n.type);
        Assert.assertEquals(m.id, n.id);
        Assert.assertArrayEquals(m.locs, n.locs);
    }

    @Test
    public void testIDGroupLocs() {
        Message m = new Message();
        m.type = MessageType.ATTACK_DEN;
        m.locs = new MapLocation[] {
            new MapLocation(-10000 + 99, 10000 - 99),
            new MapLocation(-10000 + 99, 10000)
        };
        m.group = 2;
        m.id = Messager.hash(31911, 432);

        int[] enc = Messager.encode(m);
        Message n = Messager.decode(enc[0], enc[1]);
        Assert.assertEquals(m.type, n.type);
        Assert.assertEquals(m.id, n.id);
        Assert.assertArrayEquals(m.locs, n.locs);
        Assert.assertEquals(m.group, n.group);
    }
}