package team031.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jdshen on 1/8/16.
 */
public class Math2Test {
    @Test
    public void testFloorDivision() {
        Assert.assertEquals(-2, Math2.floorDivision(-10 , 8));
        Assert.assertEquals(-11, Math2.floorDivision(-41 , 4));
        Assert.assertEquals(-7, Math2.floorDivision(-45 , 7));
    }

}